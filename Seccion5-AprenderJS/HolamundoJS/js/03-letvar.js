'use strict'
// pruebas con let y vars
var numero = 40;
console.log(numero);
if(true){
  var numero = 50;
  console.log(numero);
}
console.log(numero);

// prueba con let
var texto = "Hola";
console.log(texto);
if(true){
  let texto = "Mundo";
  console.log(texto);
}
console.log(texto);
 // con var se crea y actualiza una variable global, si se crea
 // una variable let, crea una variable local
