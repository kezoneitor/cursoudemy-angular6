'use strict'

//Operadores
var numero1 = 10;
var numero2 = 5;
const sig = "+";
var operacion;
if(sig === "+"){
  operacion = numero1 + numero2;
}else if(sig === "%"){
  operacion = numero1 % numero2;
}

alert("el resultado de " + sig + " es " + operacion);

// Tipos de datos
// En el texto o string las comillas dobles (" ") tienen mayor
// prioridad que las comillas simples (' ')
